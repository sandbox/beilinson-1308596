(function ($) {


// Behavior to load bgndgallery
Drupal.behaviors.bgndgallery = {
  attach: function(context, settings) {
    $('.bgndgallery-content', context).once('bgndgallery', function() {
      $(this).each(function() {
        var $this = $(this);
        var id = $this.attr('id');
        var optionset = settings.bgndgallery.instances[id];
        if (optionset) {
 $.mbBgndGallery.buildGallery(settings.bgndgallery.optionsets[optionset]);
        }
        else {
          $this.mbBgndGallery.buildGallery();
        }
      });
    });
  }
};

}(jQuery));
