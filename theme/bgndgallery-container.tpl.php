<?php
/**
 * @file
 * Default output for a galleria node.
*/
?>
<div class="bgndgallery-content clearfix" id="bgndgallery-<?php print $id; ?>">
<?php if ($options['controls']): ?>
<div id="controls">
	<div class="pause"><?php print t('pause'); ?></div>
	<div class="play"><?php print t('play'); ?></div>
	<div class="prev"><?php print t('prev'); ?></div>
	<div class="next"><?php print t('next'); ?></div>
	<div class="counter"></div>
</div><?php endif; ?>
</div>
