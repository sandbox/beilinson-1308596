<?php
/**
 * @file
 * Theming functions for the bgndgallery module.
 * 
 * Preprocessor functions fill variables for templates and helper
 * functions to make theming easier.
 */

/**
 * Template preprocess handler for 'bgndgallery_container' theme.
 */
function template_preprocess_bgndgallery_container(&$vars) {
  // Each bgndgallery instance gets a unique id
  $bgndgallery_id = &drupal_static('bgndgallery_id', 0);
  $vars['id'] = ++$bgndgallery_id;

  // Load the used option set
  if (!empty($vars['settings']['bgndgallery_optionset'])) {
    $optionset = bgndgallery_optionset_load($vars['settings']['bgndgallery_optionset']);
  }
  if (empty($optionset)) {
    // Fall back to 'default' option set
    $optionset = bgndgallery_optionset_load('default');
  }
  $vars['options'] = $optionset['options'];
  // Attach bgndgallery JavaScript
 // bgndgallery_add_js($bgndgallery_id, $optionset);

  // Prepare image elements
  $items = $vars['items'];
  $vars['items'] = array();
  foreach ($items as $delta => $item) {
    // Get URL for "normal" image
    if (empty($optionset['imagestyle_size'])) {
       $normal_url = file_create_url($item['uri']);
    }
    else {
       $normal_url = image_style_url($optionset['imagestyle_size'], $item['uri']);
    }

   
    $pics[] = $vars['pics'][$delta] = $normal_url;
  }
$optionset['options']['images']=$pics;
  bgndgallery_add_css();
  bgndgallery_add_js($bgndgallery_id, $optionset);
}
