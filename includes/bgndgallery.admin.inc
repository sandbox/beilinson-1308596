<?php
/**
 * @file
 * Administrative page callbacks for the bgndgallery module.
 */


/**
 * Menu callback; Listing of all current option sets.
 */
function bgndgallery_page_optionset_list() {
  $optionsets = bgndgallery_optionsets();

  $header = array(t('Option Set Name'), array('data' => t('Operations'), 'colspan' => 2));
  $rows = array();
  foreach ($optionsets as $name => $optionset) {
    $rows[] = array(
      l($optionset['title'], 'admin/config/media/bgndgallery/edit/' . $name),
      l(t('edit'), 'admin/config/media/bgndgallery/edit/' . $name),
      // Hide the delete link for the 'default' set
      ($name == 'default') ? '' : l(t('delete'), 'admin/config/media/bgndgallery/delete/' . $name),
    );
  }

  return theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'empty' => t('There are currently no option sets. <a href="!url">Add a new one</a>.', array('!url' => url('admin/config/media/bgndgallery/add'))),
  ));
}


/**
 * Form builder; Form for adding a new option set.
 */
function bgndgallery_form_optionset_add($form, &$form_state) {
  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => '255',
    '#machine_name' => array(
      'source' => array('title'),
      'exists' => 'bgndgallery_optionset_exists',
    ),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Create new option set'),
    ),
    'cancel' => array(
      '#type' => 'link',
      '#title' => t('Cancel'),
      '#href' => 'admin/config/media/bgndgallery',
    )
  );

  return $form;
}

/**
 * Submit handler for adding a new option set.
 */
function bgndgallery_form_optionset_add_submit($form, &$form_state) {
  $optionset = array(
    'name' => $form_state['values']['name'],
    'title' => $form_state['values']['title'],
    'options' => array(
      'timer' => 4000,
      'effTimer' => 2000,
      'containment' => 'body',
      'controls' => '#controls',
      'raster' => '/sites/all/libraries/bgndgallery/inc/raster.png',
      'autoStart' => TRUE,
      'activateKeyboard' => TRUE,
      'preserveTop' => FALSE,
    ),
  );
  $optionset = bgndgallery_optionset_save($optionset, TRUE);
  drupal_set_message(t('Option set %name was created.', array('%name' => $optionset['name'])));
  $form_state['redirect'] = 'admin/config/media/bgndgallery/edit/' . $optionset['name'];
}


/**
 * Theme to embed tables into forms.
 */
function theme_bgndgallery_form_table($variables) {
  $form = $variables['form'];

  $rows = array();
  foreach (element_children($form) as $row_key) {
    $row = array();
    foreach (element_get_visible_children($form[$row_key]) as $cell_key) {
      $cell = array('data' => drupal_render($form[$row_key][$cell_key]));
      if (!empty($form[$row_key][$cell_key]['#table_attributes']))
        $cell += $form[$row_key][$cell_key]['#table_attributes'];
      $row[] = $cell;
    }
    $rows[] = $row;
  }

  $variables = array();
  foreach ($form as $key => $value) {
    if (element_property($key)) {
      $variables[substr($key, 1)] = $value;
    }
  }
  $variables['rows'] = $rows;

  return theme('table', $variables);
}

/**
 * This function returns an array defining the form elements used to edit the different options.
 */
function bgndgallery_option_elements() {
  $file = libraries_get_path('bgndgallery');
  return array(
    'timer' => array(
      '#type' => 'textfield',
      '#title' => t('Timer'),
      '#description' => t('The value in millisecond for the display duration'),
      '#element_validate' => array('_bgndgallery_validate_integer'),
      '#default_value' => 4000,
    ),
    'effTimer' => array(
      '#type' => 'textfield',
      '#title' => t('Effects Timer'),
      '#description' => t('The value in millisecond for the transaction duration'),
      '#element_validate' => array('_bgndgallery_validate_integer'),
      '#default_value' => 2000,
    ),
    'containment' => array(
      '#type' => 'textfield',
      '#title' => t('Containment'),
      '#description' => t('The selector of the element where the gallery will be inserted'),
      '#default_value' => 'body',
    ),
    'controls' => array(
      '#type' => 'textfield',
      '#title' => t('Controls'),
      '#description' => t('The selector of the element where the controls will be inserted'),
      '#default_value' => '#controls',
    ),
    'raster' => array(
      '#type' => 'textfield',
      '#title' => t('Raster'),
      '#description' => t('The path to the raster image, if the gallery has a raster overlay smaller images are better displayed'),
      '#default_value' => $file . '/inc/raster.png',
    ),
    'autoStart' => array(
      '#type' => 'checkbox',
      '#title' => t('Auto Start'),
      '#description' => t('Define if the gallery should start once initialized'),
      '#default_value' => TRUE,
    ),
    'activateKeyboard' => array(
      '#type' => 'checkbox',
      '#title' => t('Thumbnail Carousel'),
      '#description' => t('Define if the gallery should be navigable via keyboard'),
      '#default_value' => TRUE,
    ),
    'preserveTop' => array(
      '#type' => 'checkbox',
      '#title' => t('Thumbnail Carousel'),
      '#description' => t('Define if the images should preserve their top position or centered'),
      '#default_value' => FALSE,
    ),
  );
}

/**
 * Returns the form element to use to edit the given option.
 */
function bgndgallery_option_element($option, $value) {
  $elements = bgndgallery_option_elements();
  $element = isset($elements[$option]) ? $elements[$option] : array('#type' => 'textfield');

  if ($value !== NULL) {
    if ($element['#type'] == 'select') {
      if ($value === TRUE) {
        $value = 'true';
        }
      elseif ($value === FALSE) {
        $value = 'false';
        }
    }
    $element['#default_value'] = $value;
  }

  return $element;
}

/**
 * Form  builder; Form to edit a given option set.
 */
function bgndgallery_form_optionset_edit($form, &$form_state, $optionset) {
  if (empty($form_state['optionset'])) {
    $form_state['optionset'] = $optionset;
  }
  else {
    $optionset = $form_state['optionset'];
  }

 /* // Print a warning if there's no height and/or width option in the set.
  foreach (array('height', 'width') as $dimension) {
    if (!array_key_exists($dimension, $optionset['options'])) {
      drupal_set_message(t('You should add a %dimension option if you don\'t plan to set it manually via CSS.', array('%dimension' => $dimension)), 'warning');
    }
  }
*/
  // Title
  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#default_value' => $optionset['title'],
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
  );

 /* // Show the theme select box if there is more than one theme
  $themes = array_keys(bgndgallery_get_themes(TRUE));
  if (count($themes) == 1) {
    $form['theme'] = array(
      '#type' => 'hidden',
      '#value' => $themes[0],
    );
  }
  elseif (count($themes) > 1) {
    asort($themes);

    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#description' => t('As you have more than one bgndgallery theme installed, please select the one to use.'),
      '#options' => array_combine($themes, $themes),
      '#default_value' => $optionset['theme'],
    );
  }
*/
  // Show select boxes for the various image styles (thumbnail, normal, big)
  $image_styles = image_style_options(FALSE);
  $form['image_styles'] = array(
    '#type' => 'fieldset',
    '#title' => 'Image styles',
    '#tree' => TRUE,
  );
  $form['image_styles']['size'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail image style'),
    '#description' => t('Image style for the thumbnail bar.'),
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
    '#default_value' => $optionset['imagestyle_size'],
  );

  // Option table
  $form['options'] = array(
    '#theme' => 'bgndgallery_form_table',
    '#tree' => TRUE,
    '#header' => array(t('Name'), t('Value'), t('Operations')),
  );

  $i = 0;
  foreach ($optionset['options'] as $key => $value) {
    $option_element = bgndgallery_option_element($key, $value);

    $form['options'][] = array(
      'name' => array(
        '#type' => 'item',
        '#title' => check_plain($key),
        '#description' => isset($option_element['#title']) ? $option_element['#title'] : '',
      ),
      'value' => $option_element + array(
        '#option_name' => $key,
        '#title_display' => 'none',
      ),
      'delete' => array(
        '#type' => 'submit',
        '#name' => 'button_del_' . $i++,
        '#value' => t('Delete'),
        '#submit' => array('bgndgallery_form_optionset_edit_submit_delete'),
        '#limit_validation_errors' => array(),
      ),
    );
  }

  // 'Add option' row at the end of the table
  $options = array_diff(array_keys(bgndgallery_option_elements()), array_keys($optionset['options']));
  $options = empty($options) ? array() : array_combine($options, $options);
  $form['options'][] = array(
    'add_option_row' => array(
      '#table_attributes' => array('colspan' => '3', 'class' => array('container-inline')),
      '#tree' => FALSE,
      'new_option' => array(
        '#type' => 'select',
        '#options' => $options,
        '#empty_option' => t('Select or enter:'),
      ),
      'new_option_custom' => array(
        '#type' => 'textfield',
        '#states' => array(
          'visible' => array(
            ':input[name="new_option"]' => array('value' => ''),
          ),
        ),
      ),
      'button_add' => array(
        '#type' => 'submit',
        '#name' => 'add_option',
        '#value' => t('Add option'),
        '#submit' => array('bgndgallery_form_optionset_edit_submit_add'),
        '#limit_validation_errors' => array(
          array('new_option'),
          array('new_option_custom'),
        ),
      ),
    ),
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#name' => 'submit',
      '#value' => t('Save option set')
    ),
    'cancel' => array(
      '#type' => 'link',
      '#title' => t('Cancel'),
      '#href' => 'admin/config/media/bgndgallery',
    )
  );

  return $form;
}

/**
 * Validate a form element that should have an integer value.
 */
function _bgndgallery_validate_integer($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer.', array('%name' => $element['#title'])));
  }
}

/**
 * Validate a form element that should have an integer value or the special value 'auto'.
 */
function _bgndgallery_validate_integer_auto($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && $value !== 'auto' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer or the special value \'auto\'.', array('%name' => $element['#title'])));
  }
}

/**
 * Validate a form element that should have a number as value.
 */
function _bgndgallery_validate_number($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && !is_numeric($value)) {
    form_error($element, t('%name must be a number.', array('%name' => $element['#option_name'])));
  }
}

/**
 * Validate a form element that should have a value between 0 and 1.
 */
function _bgndgallery_validate_opacity($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && (!is_numeric($value) || $value < 0 || $value > 1)) {
    form_error($element, t('%name must be a value between 0 and 1.', array('%name' => $element['#option_name'])));
  }
}

/**
 * Validate a form element that should have an integer value or the special value 'all'.
 */
function _bgndgallery_validate_preload($element, &$form_state) {
  $value = $element['#value'];
  if ($value !== '' && $value !== 'all' && (!is_numeric($value) || intval($value) != $value || $value < 0)) {
    form_error($element, t('%name must be a positive integer or the special value \'all\'.', array('%name' => $element['#option_name'])));
  }
}

/**
 * Submit handler for 'Add option' button; Add a new option to the set.
 */
function bgndgallery_form_optionset_edit_submit_add($form, &$form_state) {
  $optionset = &$form_state['optionset'];

//  $optionset['options'] = array_fill_keys(array_keys(bgndgallery_option_elements()), NULL);

  if (!empty($form_state['values']['new_option'])) {
    $new_option_element = 'new_option';
  }
  elseif (!empty($form_state['values']['new_option_custom'])) {
    $new_option_element = 'new_option_custom';
  }

  if (isset($new_option_element)) {
    $new_option = $form_state['values'][$new_option_element];
    if (!array_key_exists($new_option, $optionset['options'])) {
      // Add the new option with a NULL value. The input element cares for a default value.
      $optionset['options'][$new_option] = NULL;
      // Reset the input field
      $form_state['input'][$new_option_element] = '';
      drupal_set_message(t('Option %name added.', array('%name' => $new_option)));
    }
    else {
      form_set_error($new_option_element, t('This set already includes the %name option.', array('%name' => $new_option)));
    }
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for 'Delete' buttons; Delete an option from the set.
 */
function bgndgallery_form_optionset_edit_submit_delete($form, &$form_state) {
  $optionset = &$form_state['optionset'];

  $rowindex = $form_state['triggering_element']['#parents'][1];
  $option = $form['options'][$rowindex]['value']['#option_name'];

  unset($optionset['options'][$option]);
  drupal_set_message(t('Option %name removed.', array('%name' => $option)));

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for 'Save option set' button; Save the changed option set.
 */
function bgndgallery_form_optionset_edit_submit($form, &$form_state) {
  $optionset = &$form_state['optionset'];

  $optionset['title'] = $form_state['values']['title'];
  $optionset['imagestyle_size']  = $form_state['values']['image_styles']['size'];

  foreach ($form_state['values']['options'] as $index => $values) {
    $element = $form['options'][$index]['value'];
    $value  = $values['value'];

    if ($value !== '') {
      // Do some typeguessing here...
      if ($element['#type'] == 'checkbox') {
        $value = (bool) $value;
      }
      elseif (is_numeric($value)) { // || intval($value) != $value || $value < 0)
        $value = (float) $value;
      }
      elseif (strcasecmp($value, 'true') == 0) {
        $value = TRUE;
      }
      elseif (strcasecmp($value, 'false') == 0) {
        $value = FALSE;
      }
    }

    $option = $element['#option_name'];
    $optionset['options'][$option] = $value;
  }

  bgndgallery_optionset_save($optionset);
  drupal_set_message(t('Option set %name changed.', array('%name' => $optionset['name'])));
  $form_state['redirect'] = 'admin/config/media/bgndgallery';
}


/**
 * Form builder; Form to delete a given option set.
 */
function bgndgallery_optionset_form_delete($form, &$form_state, $optionset) {
  $form_state['optionset'] = &$optionset;

  return confirm_form(
    $form,
    t('Are you sure you want to delete the option set %name?', array('%name' => $optionset['name'])),
    'admin/config/media/bgndgallery',
    NULL,
    t('Delete'),  t('Cancel')
  );
}

/**
 * Submit handler for deleting an option set.
 */
function bgndgallery_optionset_form_delete_submit($form, &$form_state) {
  $optionset = &$form_state['optionset'];

  if ($optionset['name'] == 'default') {
    // Prevent deletion of the default set so we can fall back to it.
    drupal_set_message(t('The default option set may not be deleted!', 'error'));
  }
  else {
    bgndgallery_optionset_delete($optionset);
    drupal_set_message(t('Option set %name was deleted.', array('%name' => $optionset['name'])));
  }

  $form_state['redirect'] = 'admin/config/media/bgndgallery';
}


/**
 * Form builder; Form for advanced module settings.
 */
function bgndgallery_form_settings() {
  $form = array();

  $form['library'] = array(
    '#type' => 'fieldset',
    '#title' => 'Library',
  );

  $file = libraries_get_path('bgndgallery');
  if (!is_dir($file)) {
    drupal_set_message(t('Library directory not found: <code>@file</code>', array('@file' => $file)), 'error', FALSE);
  }
  $form['library']['lib_path'] = array(
    '#type' => 'item',
    '#title' => t('Library path'),
    '#markup' => t('<code>@file</code>', array('@file' => $file)),
  );

  $file = bgndgallery_get_library_file();
  $form['library']['lib_file'] = array(
    '#type' => 'item',
    '#title' => t('Library file'),
    '#markup' => $file ? t('<code>@file</code>', array('@file' => $file)) : t('Unknown'),
    '#description' => t('This filename is cached until the file is deleted.'),
  );


  $form['library']['button_clearcache'] = array(
    '#type' => 'submit',
    '#name' => 'button_clearcache',
    '#value' => t('Clear cache'),
    '#submit' => array('bgndgallery_form_settings_submit_clearcache'),
  );

  return $form;
}

/**
 * Submit handler for the advanced module settings form button 'Clear cache'.
 */
function bgndgallery_form_settings_submit_clearcache($form, &$form_state) {
  cache_clear_all('bgndgallery_', 'cache', TRUE);
  drupal_set_message(t('Cache cleared.'));
}

/**
 * Submit handler for the advanced module settings.
 */
function bgndgallery_form_settings_submit($form, &$form_state) {
  drupal_set_message(t('NYI: Nothing done.'));
}
