BgndGallery Drupal Module
======================

1. How to install BgndGallery
2. How to use BgndGallery
3. Configure BgndGallery


1. How to install BgndGallery
--------------------------

1. Make sure the Libraries module is installed.
   You can download it from it's project page at
   http://drupal.org/project/libraries
2. Download the jquery.mb.bgndGallery Javascript library from
   http://pupunzi.open-lab.com/mb-jquery-components/jquery-mb-bgndGallery
   and extract the bgndGallery folder into your /sites/all/libraries/ directory.
3. Extract the BgndGallery Drupal module into your /sites/all/modules/ directory and
   enable it (module "BgndGallery" in category "Other").


2. How to use BgndGallery
----------------------

There are several approaches of building a BgndGallery image gallery:

a) Using BgndGallery with an image/media field in a node:
   This is the classic and easiest way of creating a BgndGallery gallery.
   You create an image field and upload multiple images to it.

    1. Create a new content type for your gallery pages or alter an existing one.
    2. Add a new or existing field of type "Image" or "Multimedia asset" on the "Manage Fields" tab of the content type.
       When you save the content type, you will be redirected to the settings form of the field instance where you
       can change the allowed file extensions, image resolutions etc.
    3. Go to the "Manage Display" page of your content type. Set the "Format" of your image field to "BgndGallery".
       To the right of the select box you can now choose the option set to use for this BgndGallery instance.
       For details on option sets, see "Configure BgndGallery" below.
    4. Create a new page of your content type, upload some images and view the node.
       The images should now be rendered in a nice background slideshow.

b) Using BgndGallery with an imagenode and a nodereference gallery:
   In this mode you'll create one single node per image. An additional gallery content type will hold a
   nodereference field. Creating a gallery is a multi step process, but images can be reused across galleries.

   For this to work, you need the References module from http://drupal.org/project/references.

    1. Create your image content type or alter an existing one.
    2. Add an image field to your content type. Only allow ONE single image per node.
    3. Create your gallery content type or alter an existing one.
    4. Add a nodereference field to your content type.
       Make sure you reference the content type from step #1.
    5. Go to the "Manage Display" page of your gallery content type. Set the "Format" of your nodereference field
       to "BgndGallery". To the right of the select box you can now choose the option set to use for this BgndGallery instance.
       For details on option sets, see "Configure BgndGallery" below.
    6. Upload some images, add titles if you want. Create a gallery node, reference the images and view the node.
       You should now be able see the images in the BgndGallery display widget.


3. Configure BgndGallery
---------------------

The configuration page for the BgndGallery module can be found on the Drupal "Configuration" page within the section
"Media" (the path is admin/config/media/bgndgallery).
The main concept of configuring how your BgndGallerys look like are so called "option sets".
For each gallery, you can assign one of these option sets which determine the size, theme, image styles and
overall BgndGallery options.
For a documentation of available options, see https://github.com/pupunzi/jquery.mb.bgndGallery/wiki.
You have to at least specify one option set. You could leave default options.